import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
);

const labels = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const Chart = ({ monthlyAppointments = {}, nameOfDoctor = null }) => {
  const data = {
    labels,
    datasets: [
      {
        label: "Monthly Appointments",
        data: labels.map((_, index) => monthlyAppointments[labels[index]]),
        backgroundColor: "rgba(255, 99, 132, 0.9)",
      },
    ],
  };

  const options = {
    responsive: true,
    scales: {
      x: {
        title: {
          display: true,
          text: "Month",
        },
      },
      y: {
        title: {
          display: true,
          text: "Appointments",
        },
        min: 0,
        ticks: {
          stepSize: 1,
        },
      },
    },
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: `${
          nameOfDoctor ? `Dr. ${nameOfDoctor}` : "Doctor's"
        } Monthly Appointments`,
      },
    },
  };
  return <Bar options={options} data={data} />;
};

export default Chart;
