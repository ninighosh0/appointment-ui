import React from 'react';
import {BrowserRouter,Routes,Route} from 'react-router-dom';
import {Toaster} from "react-hot-toast";
import Login from "./pages/Login"
import Register from "./pages/Register"
import Home from './pages/Home';
import ApplyDoctor from './pages/ApplyDoctor';
import { useSelector } from 'react-redux';
import ProtectedRoutes from './components/ProtectedRoutes';
import PublicRoutes from './components/PublicRoutes';
import Notification from './pages/Notification';
import UsersList from './pages/Admin/UsersList';
import DoctorsList from './pages/Admin/DoctorsList';
import Profile from './pages/Doctor/Profile';
import CommonProfile from './pages/Profile';
import BookTime from "./components/BookTime"
import Appointments from './pages/User/Appointments';
import BookAppointment from './pages/User/BookAppointment';
import DoctorAppoints from './pages/Doctor/DoctorAppoints';
import ChangeTimings from './pages/Doctor/ChangeTimings';
import CreatePrescription from './pages/Doctor/CreatePrescription';
import EditPrescription from './pages/Doctor/EditPrescription';
import PrescriptionShow from "./pages/User/PrescriptionShow"
import PreviousAppointment from './pages/Admin/PreviousAppointment';
import DoctorPreviousAppointment from './pages/Doctor/PreviousAppointments';
import UserPreviousAppointment from './pages/User/PreviousAppointments';
import AnalyzeDoctor from './pages/Admin/AnalyzeDoctor';

function App() {
  const {loading}=useSelector((state)=>state.alerts);
  return (
    <BrowserRouter>
    {loading && (
      <div className="spinner-parent">
      <div className="spinner-border" role="status">
      </div>
      </div>
    )}
   
    <Toaster position="top-center" reverseOrder={false}/>
    <Routes>
    <Route path='/login' element={<PublicRoutes><Login/></PublicRoutes>}/>
    <Route path='/register' element={<PublicRoutes><Register/></PublicRoutes>}/>
    <Route path='/' element={<ProtectedRoutes><Home/></ProtectedRoutes>}/>
    <Route path='/apply-doctor' element={<ProtectedRoutes><ApplyDoctor/></ProtectedRoutes>}/>
    <Route path='/notifications' element={<ProtectedRoutes><Notification/></ProtectedRoutes>}/>
    <Route path='/admin/userslist' element={<ProtectedRoutes><UsersList/></ProtectedRoutes>}/>
    <Route path='/admin/doctorslist' element={<ProtectedRoutes><DoctorsList/></ProtectedRoutes>}/>
    <Route path='/admin/previous-appointments' element={<ProtectedRoutes><PreviousAppointment/></ProtectedRoutes>}/>
    <Route path='/admin/analyze-doctor' element={<ProtectedRoutes><AnalyzeDoctor /></ProtectedRoutes>} />
    <Route path='/doctor/profile' element={<ProtectedRoutes><Profile/></ProtectedRoutes>}/>
    <Route path='/doctor/booking' element={<ProtectedRoutes><BookTime/></ProtectedRoutes>}/>
    <Route path='/doctor/change-timings' element={<ProtectedRoutes><ChangeTimings/></ProtectedRoutes>}/>
    <Route path='/appointments' element={<ProtectedRoutes><Appointments /></ProtectedRoutes>} />
    <Route path='/book-appointment' element={<ProtectedRoutes><BookAppointment /></ProtectedRoutes>} />
    <Route path='/book-doctor-appointment' element={<ProtectedRoutes><DoctorAppoints /></ProtectedRoutes>} />
    <Route path='/create-prescription' element={<ProtectedRoutes><CreatePrescription /></ProtectedRoutes>} />
    <Route path='/edit-prescription' element={<ProtectedRoutes><EditPrescription /></ProtectedRoutes>} />
    <Route path='/view-prescriptions' element={<ProtectedRoutes><PrescriptionShow /></ProtectedRoutes>} />
    <Route path='/previous-appointments' element={<ProtectedRoutes><UserPreviousAppointment /></ProtectedRoutes>} />
    <Route path='/doctor/previous-appointments' element={<ProtectedRoutes><DoctorPreviousAppointment /></ProtectedRoutes>} />
    <Route path='/profile' element={<ProtectedRoutes><CommonProfile /></ProtectedRoutes>} />
    </Routes>
    </BrowserRouter>
  );
}

export default App;
