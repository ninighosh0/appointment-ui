import { createSlice } from "@reduxjs/toolkit";

export const prescriptionsSlice = createSlice({
  name: "prescriptions",
  initialState: {
    prescriptions:null,
    reloadUser:true
  },
  reducers: {
    setPrescriptions:(state,action)=>{
        state.prescriptions=action.payload
    },
    reloadUserData:(state,action)=>{
      state.reloadUser=action.payload
    }
  },
});

export const { setPrescriptions,reloadUserData } = prescriptionsSlice.actions;