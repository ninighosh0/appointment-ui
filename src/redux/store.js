import { configureStore } from '@reduxjs/toolkit'
import {combineReducers} from "redux"
import {alertsSlice} from "./alertsSlice"
import {usersSlice} from "./usersSlice"
import {prescriptionsSlice} from "./prescriptionsSlice"

const rootReducer=combineReducers({
    alerts: alertsSlice.reducer,
    users: usersSlice.reducer,
    prescriptions: prescriptionsSlice.reducer
});

const store=configureStore({
    reducer:rootReducer
});

export default store;