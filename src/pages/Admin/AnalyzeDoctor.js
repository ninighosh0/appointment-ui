import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import { useSearchParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { hideLoading, showLoading } from "../../redux/alertsSlice";
import axios from "axios";
import Chart from "../../components/Chart";
import html2canvas from "html2canvas";
import { Button } from "antd";
const { jsPDF } = require("jspdf");

const AnalyzeDoctor = () => {
  const [params] = useSearchParams();
  const doctorId = params.get("doctorId");
  const [loading, setLoading] = useState(true);
  const [doctor, setDoctor] = useState(null);
  const [monthlyAppointments, setMonthlyAppointments] = useState(null);
  const dispatch = useDispatch();

  const getAnalyzedDoctorDetails = async () => {
    try {
      dispatch(showLoading());
      const response = await axios.get(
        `/api/admin/analyze-doctor?doctorId=${doctorId}`,
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      setDoctor(response.data.doctor);
      setMonthlyAppointments(response.data.monthlyAppointments);
      setLoading(false);
      dispatch(hideLoading());
    } catch (error) {
      setLoading(false);
      dispatch(hideLoading());
      console.log(error);
    }
  };

  const div2PDF = (e) => {
    let input = window.document.getElementsByClassName("div2PDF")[0];
    html2canvas(input).then((canvas) => {
      const img = canvas.toDataURL("image/png");
      const pdf = new jsPDF("landscape", "pt");
      console.log(100, 100, 700, 200);
      //   input.offsetLeft - 200,
      //     input.offsetTop - 200,
      //     input.clientWidth - 500,
      //     input.clientHeight - 500,
      pdf.addImage(img, "png", 70, 100, 700, 400);
      pdf.save("doctor_appointments.pdf");
    });
  };

  useEffect(() => {
    getAnalyzedDoctorDetails();
  }, []);

  console.log(doctor, monthlyAppointments);
  return (
    <Layout>
      <h2>Analyze Doctor</h2>
      {loading ? (
        "Loading..."
      ) : doctor && monthlyAppointments ? (
        <div>
          <div style={{ marginBottom: 50 }}>
            <div>
              Name: Dr. {doctor.userId.firstName} {doctor.userId.lastName}
            </div>
            <div>Email: {doctor.userId.email}</div>
          </div>
          <div className="div2PDF">
            <Chart
              monthlyAppointments={monthlyAppointments}
              nameOfDoctor={`${doctor.userId.firstName} ${doctor.userId.lastName}`}
            />
          </div>
          <Button onClick={(e) => div2PDF(e)}>Dowload PDF</Button>
        </div>
      ) : (
        <></>
      )}
    </Layout>
  );
};

export default AnalyzeDoctor;
