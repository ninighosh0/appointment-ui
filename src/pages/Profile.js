import React from "react";
import { useSelector } from "react-redux";
import Layout from "../components/Layout";
import { Col, Form, Input, Row } from "antd";

const Profile = () => {
  const { users = null } = useSelector((state) => state.users);
  console.log(users);

  return (
    <Layout>
      {users && (
        <React.Fragment>
          <h1 className="page-title">
            {users.role === 2 ? "Admin" : "User"} Profile
          </h1>
          <Form layout="vertical" style={{ maxWidth: 1500 }}>
            <Row gutter={40}>
              <Col span={8} xs={24} sm={24} lg={8}>
                <Form.Item label="First Name" name="firstName">
                  <Input
                    placeholder="First Name"
                    defaultValue={users.firstName}
                    disabled
                  />
                </Form.Item>
              </Col>
              <Col span={8} xs={24} sm={24} lg={8}>
                <Form.Item label="Last Name" name="lastName">
                  <Input
                    placeholder="Last Name"
                    defaultValue={users.lastName}
                    disabled
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={40}>
              <Col span={8} xs={24} sm={24} lg={8}>
                <Form.Item label="Email" name="email">
                  <Input
                    placeholder="Email"
                    defaultValue={users.email}
                    disabled
                  />
                </Form.Item>
              </Col>
              <Col span={8} xs={24} sm={24} lg={8}>
                <Form.Item label="Role" name="role">
                  <Input
                    placeholder="Role"
                    defaultValue={users.role === 2 ? "ADMIN" : "USER"}
                    disabled
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={40}>
              <Col span={8} xs={24} sm={24} lg={8}>
                <Form.Item label="Profile Created At" name="createdAt">
                  <Input
                    placeholder="Profile Created At"
                    defaultValue={new Date(
                      users.createdAt,
                    ).toLocaleDateString()}
                    disabled
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </React.Fragment>
      )}
    </Layout>
  );
};
export default Profile;
