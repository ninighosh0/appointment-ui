import React,{useState} from "react";
import { Button, Col, Form, Input, Row,Select} from "antd";
import Layout from "../../components/Layout"
import { useNavigate } from "react-router-dom";
import { toast } from "react-hot-toast";
import axios from "axios";

const CreatePrescription=()=>{
    const [data,setData]=useState(JSON.parse(localStorage.getItem("prescriptions")));
    const { TextArea } = Input;
    const { Option } = Select;
    const navigate=useNavigate();

    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
          <Select
            style={{
              width: 70,
            }}
          >
            <Option value="+91">+91</Option>
          </Select>
        </Form.Item>
      );
      const onFinish=async(vals)=>{
        try{
          vals.userId=data.a.user._id
          vals.doctorId=data.id
          vals.timeId=data.a.timeSlot._id
          vals.firstName=vals.firstName ? vals.firstName : data.a.user.firstName
          vals.lastName=vals.lastName ? vals.lastName : data.a.user.lastName
          if(vals.phone.length===10 && /^\d+$/.test(vals.phone))
          {
            const response = await axios.post(
              "/api/user/create-prescription",
              vals,
              {
                headers: {
                  Authoriation: "Bearer " + localStorage.getItem("token"),
                },
              }
            );
              if(response.data.success)
              {
                  toast.success("success")
                  navigate("/book-doctor-appointment")
              }
          }   
          else
          {
              toast.error("Enter phone number correctly");
          }
        }
        catch(err)
        {
          console.log(err)
          toast.error("not created")
        }
        }
       
    return (
        <Layout>
        {data && (
            <Form
          layout="vertical"
          onFinish={onFinish}
        >
            <h1 className="card-title mt-3">Personal Information</h1>
          <Row gutter={20}>
          <Col span={8} xs={24} sm={24} lg={8}>
          <Form.Item
            label="First Name"
            name="firstName"
          >
            <Input 
            defaultValue={data?.a?.user?.firstName}
            placeholder="First Name" />
          </Form.Item>
        </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Last Name"
                name="lastName"
              >
                <Input 
                defaultValue={data?.a?.user?.lastName}
                placeholder="Last Name" />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
            <Form.Item
            name="phone"
            label="Phone Number"
            rules={[
              {
                required: true,
                message: 'Please input your phone number!',
              },
            ]}
          >
            <Input
              addonBefore={prefixSelector}
              style={{
                width: '100%',
              }}
            />
          </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                required
                label="Roll Number"
                name="rollNumber"
                rules={[{ required: true }]}
              >
                <Input placeholder="Roll Number" />
              </Form.Item>
            </Col>
          </Row>
          <hr />
          <h1 className="card-title mt-3">Problem Information</h1>
          <Row gutter={20}>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                required
                label="Problem"
                name="problem"
                rules={[{ required: true }]}
              >
                <Input placeholder="problem" />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Medical History"
                name="history"
              >
                <TextArea placeholder="history" />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Problem Description"
                name="problem_description"
              >
              <TextArea placeholder="Description" />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                required
                label="Solution"
                name="solution"
                rules={[{ required: true }]}
              >
              <TextArea placeholder="Solution" />
              </Form.Item>
            </Col>
          </Row>
    
          <div className="d-flex justify-content-end">
            <Button className="primary-button" htmlType="submit">
              SUBMIT
            </Button>
          </div>
        </Form>
        )}
       
        </Layout>
    )
}

export default CreatePrescription;