import React, { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Layout from "../../components/Layout";
import { Button } from "antd";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { hideLoading, showLoading } from "../../redux/alertsSlice";
import { toast } from "react-hot-toast";

const ChangeTimings = () => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const { users } = useSelector((state) => state.users);
  const to = searchParams.get("to");
  const from = searchParams.get("from");
  const type = searchParams.get("type");
  const timeId = searchParams.get("prevTimeId");
  const doctorId = searchParams.get("doctorId");
  const userId = searchParams.get("userId");
  const dispatch = useDispatch();

  useEffect(() => {
    if (!to || !from || !timeId || !doctorId || !userId || !type) {
      navigate("/");
    }
    if (users && users.role !== 1) {
      navigate("/");
    }
  }, [users]);

  const onClick = () => {
    if (type === "accept") {
      onAccept();
    } else if (type === "reject") {
      onReject();
    }
    return;
  };

  const onAccept = async () => {
    try {
      console.log("CALL ON ACCEPT", to, from, timeId, doctorId, userId);
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/accept-timing",
        {
          timeId,
          doctorId,
          userId,
          to,
          from,
          type,
        },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      console.log(response.data);
      if (response.data.message === "success") {
        dispatch(hideLoading());
        navigate("/");
        toast.success(response.data.message);
      }
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
    }
  };

  const onReject = async () => {
    try {
      console.log("CALL ON REJECT", to, from, timeId, doctorId, userId);
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/reject-timing",
        {
          timeId,
          doctorId,
          userId,
          to,
          from,
          type,
        },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      console.log(response.data);
      if (response.data.message === "success") {
        dispatch(hideLoading());
        navigate("/");
        toast.success(response.data.message);
      }
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
    }
  };

  const renderContent = () => {
    return (
      <React.Fragment>
        {type === "accept" ? (
          <>Are you sure to change the slot to {`[${to} - ${from}]`} ?</>
        ) : (
          <>Are you sure to reject the slot {`[${to} - ${from}]`} ?</>
        )}
      </React.Fragment>
    );
  };

  return (
    <Layout>
      {type && (
        <React.Fragment>
          <h1 className="page-title">
            {type === "accept"
              ? "Accept Timing"
              : type === "reject"
              ? "Reject Timing"
              : ""}
          </h1>
          <hr />
          {renderContent()}
          <br />
          <br />
          <Button onClick={onClick}>
            {type === "accept" ? "Accept" : "Reject"}
          </Button>
        </React.Fragment>
      )}
    </Layout>
  );
};

export default ChangeTimings;
