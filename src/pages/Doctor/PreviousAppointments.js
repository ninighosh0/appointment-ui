import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import axios from "axios";
import { useDispatch } from "react-redux";
import { hideLoading, showLoading } from "../../redux/alertsSlice";
import { Button } from "antd";
import { toast } from "react-hot-toast";

const PreviousAppointment = () => {
  const [appointments, setAppointments] = useState([]);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const fetchAppointments = async () => {
    dispatch(showLoading());
    const response = await axios.get("/api/user/doctor-previous-appointments", {
      headers: {
        Authoriation: "Bearer " + localStorage.getItem("token"),
      },
    });
    console.log(response.data.appointments);
    setAppointments(response.data.appointments);
    dispatch(hideLoading());
    setLoading(false);
  };

  useEffect(() => {
    try {
      fetchAppointments();
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
      setLoading(false);
    }
  }, []);
  return (
    <Layout>
      {loading ? (
        "Loading..."
      ) : appointments.length === 0 ? (
        "No appointments to show!"
      ) : (
        <div>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>
              <h3 className="page-header">Appointments</h3>
            </div>
          </div>
          {appointments.map((appointment, index) => (
            <div
              key={index}
              style={{
                border: "1px solid gray",
                padding: 10,
                marginBottom: 10,
                borderRadius: 10,
              }}
            >
              <div>
                Doctor Name: Dr. {appointment.doctor.userId.firstName}{" "}
                {appointment.doctor.userId.lastName}
              </div>
              <div>
                User Name: Dr. {appointment.user.firstName}{" "}
                {appointment.user.lastName}
              </div>
              <div>Date: {appointment.date}</div>
              <div>
                Time Slot:{" "}
                {`[ ${appointment.time.to} - ${appointment.time.from} ]`}
              </div>
            </div>
          ))}
        </div>
      )}
    </Layout>
  );
};

export default PreviousAppointment;
