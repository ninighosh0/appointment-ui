import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import toast from "react-hot-toast";
import { Button } from "antd";
import { hideLoading, showLoading } from "../../redux/alertsSlice";

const DoctorAppoints = () => {
  const [appointments, setAppointments] = useState([]);
  const [adata, setAdata] = useState([]);
  const [doct, setDoct] = useState();
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state);
  const navigate = useNavigate();

  const getDoctData = async () => {
    try {
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/get-doctor-bookedappointments",
        {},
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      dispatch(hideLoading());
      if (response.data.success) {
        setDoct(response.data.data);
        console.log("docts", response.data.data);
        setAppointments(response.data.data.bookedAppointment);
        if (response.data.data.bookedAppointment.length > 0) {
          let datas = response.data.data.bookedAppointment;
          let cp = [];
          const ress = await axios.post(
            "/api/user/get-prescription-by-id",
            {
              doctId: response.data.data._id,
              datas,
            },
            {
              headers: {
                Authoriation: "Bearer " + localStorage.getItem("token"),
              },
            },
          );

          if (ress.data.success) {
            setAdata(ress.data.data);
          }
        }
      }
    } catch (err) {
      console.log(err);
      dispatch(hideLoading());
    }
  };

  const onDelete = async ({ userId = null, timeId = null }) => {
    try {
      if (!userId || !timeId) {
        return;
      }
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/delete-doctor-appointment",
        {
          timeId,
          userId,
        },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      if (response.data.message) {
        toast.success(response.data.message);
        setTimeout(() => window.location.reload(), 1000);
      } else {
        dispatch(hideLoading());
        toast.error(response.data.message);
      }
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
    }
  };

  const getDetails = async (ind) => {
    try {
      const gres = await axios.post(
        "/api/user/get-prescription-one",
        {
          id: ind,
        },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      if (gres.data.success) {
        localStorage.setItem("prescriptions", JSON.stringify(gres.data.data));
        navigate("/edit-prescription");
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getDoctData();
  }, []);

  return (
    <Layout>
      {loading ? (
        "Loading..."
      ) : appointments.length === 0 ? (
        "No Appointments"
      ) : (
        <React.Fragment>
          {appointments.length === 0 ? (
            "No appointments"
          ) : (
            <div>
              {appointments.map((a, index) => (
                <div
                  style={{
                    border: "1px solid gray",
                    borderRadius: 5,
                    padding: 10,
                    marginTop: 10,
                    marginBottom: 10,
                    cursor: "pointer",
                  }}
                  key={index}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <div>
                      Patient Name: {`${a.user.firstName} ${a.user.lastName}`}
                    </div>
                    <div>
                      <Button
                        onClick={() =>
                          onDelete({
                            timeId: a.timeSlot._id,
                            userId: a.user._id,
                          })
                        }
                      >
                        <i
                          className="ri-delete-bin-line"
                          color="red"
                          style={{ fontSize: 15 }}
                        />
                      </Button>
                    </div>
                  </div>
                  <div>
                    Time Slot: {`${a.timeSlot.to}- ${a.timeSlot.from}`}
                  </div>
                  <div style={{ marginTop: 20 }}>
                    {adata && adata[index] === 0 ? (
                      <Button
                        onClick={() => {
                          const val = {
                            id: doct._id,
                            a,
                          };
                          localStorage.setItem(
                            "prescriptions",
                            JSON.stringify(val),
                          );
                          navigate("/create-prescription");
                        }}
                      >
                        Create Prescription
                      </Button>
                    ) : (
                      <Button
                        onClick={() => {
                          getDetails(adata[index]);
                        }}
                      >
                        Edit Prescription
                      </Button>
                    )}
                  </div>
                </div>
              ))}
            </div>
          )}
        </React.Fragment>
      )}
    </Layout>
  );
};

export default DoctorAppoints;
