import React,{useEffect, useState} from "react";
import { Button, Col, Form, Input, Row,Select} from "antd";
import Layout from "../../components/Layout"
import { useNavigate } from "react-router-dom";
import { toast } from "react-hot-toast";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import axios from "axios";

const EditPrescription=()=>{
    const [data,setData]=useState(JSON.parse(localStorage.getItem("prescriptions")));
    const [role,setRole]=useState(false)
    const { TextArea } = Input;
    const { Option } = Select;
    const navigate=useNavigate();

    const getRoles=async()=>{
      try{
        const rr=await axios.post("/api/user/check-role",{},{
            headers: {
              Authoriation: "Bearer " + localStorage.getItem("token"),
            },
        })
        if(rr.data.success)
        {
          setRole(rr.data.data)
        }
      }
      catch(err)
      {
        console.log(err)
      }
    }
    useEffect(()=>{
       getRoles()
    },[])


    const downloadTxtFile = () => {
        html2canvas(document.querySelector("#capture")).then(canvas => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF();
            const imgProps= pdf.getImageProperties(canvas);
            const pdfWidth = pdf.internal.pageSize.getWidth();
            const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
            pdf.addImage(imgData, 'PNG', 0, 0,pdfWidth,pdfHeight);
            pdf.save("download.pdf"); 
        });
    }
 
    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
          <Select
            style={{
              width: 70,
            }}
            defaultValue={"+91"}
          >
            <Option value="+91">+91</Option>
          </Select>
        </Form.Item>
      );
      const onFinish=async(vals)=>{
        try{
          vals.userId=data.userId
          vals.doctorId=data.doctorId
          vals.timeSlot=data.timeSlot
          vals.firstName=vals.firstName ? vals.firstName : data.firstName
          vals.lastName=vals.lastName ? vals.lastName : data.lastName
          vals.phone=vals.phone ? vals.phone : data.phone
          vals.rollNumber=vals.rollNumber ? vals.rollNumber : data.rollNumber
          vals.problem=vals.problem ? vals.problem : data.problem
          vals.solution=vals.solution ? vals.solution : data.solution
          vals.problem_description=vals.problem_description ? vals.problem_description : data.problem_description
          vals.history=vals.history ? vals.history : data.history
          vals.id=data._id

          if(vals.phone.length===10 && /^\d+$/.test(vals.phone))
          {
            const response = await axios.post(
              "/api/user/edit-prescription",
              vals,
              {
                headers: {
                  Authoriation: "Bearer " + localStorage.getItem("token"),
                },
              }
            );
              if(response.data.success)
              {
                  toast.success("success")
                  navigate("/book-doctor-appointment")
              }
          }   
          else
          {
              toast.error("Enter phone number correctly");
          }
        }
        catch(err)
        {
          console.log(err)
          toast.error("not created")
        }
        }

        console.log(role,"role")
       
    return (
        <Layout>
        {data && (
            <Form
          layout="vertical"
          onFinish={onFinish}
        >
        <div
        id="capture"
        >
            <h1 className="card-title mt-3">Personal Information</h1>
            {role ?
              <Row gutter={20}>
              <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="First Name"
                name="firstName"
              >
                <Input 
                defaultValue={data?.firstName}
                placeholder="First Name" />
              </Form.Item>
            </Col>
                <Col span={8} xs={24} sm={24} lg={8}>
                  <Form.Item
                    label="Last Name"
                    name="lastName"
                  >
                    <Input 
                    defaultValue={data?.lastName}
                    placeholder="Last Name" />
                  </Form.Item>
                </Col>
                <Col span={8} xs={24} sm={24} lg={8}>
                <Form.Item
                name="phone"
                label="Phone Number"
              >
                <Input
                  addonBefore={prefixSelector}
                  defaultValue={data?.phone}
                  style={{
                    width: '100%',
                  }}
                />
              </Form.Item>
                </Col>
                <Col span={8} xs={24} sm={24} lg={8}>
                  <Form.Item
                    label="Roll Number"
                    name="rollNumber"
                  >
                    <Input placeholder="Roll Number"
                    defaultValue={data?.rollNumber}
                    />
                  </Form.Item>
                </Col>
              </Row>
              :
              <Row gutter={20}>
              <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
              label="FirstName"
              >
              <Input 
              value={data?.firstName}
              placeholder="First Name" />   
              </Form.Item> 
              </Col>
              <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
              label="LastName"
              >
              <Input 
              value={data?.lastName}
              placeholder="Last Name" />
              </Form.Item>
              
              </Col>
              <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
              label="Phone"
              >
              <Input
              addonBefore={prefixSelector}
              value={data?.phone}
              style={{
                width: '100%',
              }}
              />
              </Form.Item>
              </Col>
              <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
              label="Phone"
              >
              <Input placeholder="Roll Number"
              value={data?.rollNumber}
              />
              </Form.Item>
            </Col>
          </Row>
            }
          
          <hr />
          <h1 className="card-title mt-3">Problem Information</h1>
          {role ?
            <Row gutter={20}>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Problem"
                name="problem"
              >
                <Input placeholder="problem"
                defaultValue={data?.problem}
                />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Medical History"
                name="history"
              >
                <TextArea placeholder="history" 
                defaultValue={data?.history}
                />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Problem Description"
                name="problem_description"
              >
              <TextArea placeholder="Description"
              defaultValue={data?.problem_description}
              />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
              <Form.Item
                label="Solution"
                name="solution"
              >
              <TextArea placeholder="Solution"
              defaultValue={data?.solution}
              />
              </Form.Item>
            </Col>
          </Row>
          :
          <Row gutter={20}>
            <Col span={8} xs={24} sm={24} lg={8}>
            <Form.Item
              label="Problem"
              >
              <Input 
              value={data?.problem}
              />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
            <Form.Item
              label="History"
              >
              <TextArea placeholder="history" 
                value={data?.history}
                />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
            <Form.Item
              label="Problem_Description"
              >
              <TextArea placeholder="Description"
              value={data?.problem_description}
              />
              </Form.Item>
            </Col>
            <Col span={8} xs={24} sm={24} lg={8}>
            <Form.Item
              label="Solution"
              >
              <TextArea placeholder="Solution"
              value={data?.solution}
              />
              </Form.Item>
            </Col>
          </Row>
          }
          </div>
          {role && 
          <div className="d-flex justify-content-end">
            <Button className="primary-button" htmlType="submit">
              SUBMIT
            </Button>
          </div>
          }
          
          <div className="d-flex justify-content-end mt-1">
            <Button className="primary-button"
            onClick={downloadTxtFile} 
            >
              DOWNLOAD
            </Button>
          </div>
        </Form>
        )}
       
        </Layout>
    )
}

export default EditPrescription;