import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "antd";
import { hideLoading, showLoading } from "../../redux/alertsSlice";
import toast from "react-hot-toast";

const AppointmentCard = ({ appointment = null }) => {
  const dispatch = useDispatch();
  const onFinish = async ({ to, from, doctorId, timeId }) => {
    try {
      console.log(to, from, doctorId, timeId);
      if (!to || !from || !doctorId || !timeId) return;
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/request-time",
        {
          to,
          from,
          doctorId,
          timeId,
        },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      console.log(response.data);
      dispatch(hideLoading());
      toast.success(response.data.message);
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
    }
  };

  const onDelete = async ({ doctorId = null, timeId = null }) => {
    try {
      if (!doctorId || !timeId) {
        return;
      }
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/delete-appointment",
        {
          doctorId,
          timeId,
        },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      if (response.data.message) {
        toast.success(response.data.message);
        setTimeout(() => window.location.reload(), 1000);
      } else {
        dispatch(hideLoading());
        toast.error(response.data.message);
      }
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
    }
  };
  return (
    <div
      style={{
        border: "1px solid gray",
        borderRadius: 5,
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        cursor: "pointer",
      }}
    >
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div>
          Doctor Name:{" "}
          {`${appointment.doctor.userId.firstName} ${appointment.doctor.userId.lastName}`}
        </div>
        <div>
          <Button
            onClick={() =>
              onDelete({
                doctorId: appointment.doctor._id,
                timeId: appointment.timeSlot._id,
              })
            }
          >
            <i
              className="ri-delete-bin-line"
              color="red"
              style={{ fontSize: 15 }}
            />
          </Button>
        </div>
      </div>
      <div>
        Time Slot: {`${appointment.timeSlot.to} - ${appointment.timeSlot.from}`}
      </div>
      <div>
        <h5 className="mt-3" style={{ fontWeight: "bold" }}>
          Request for change of timings
        </h5>
        {appointment.doctor.appointment.map((docAppointment) => (
          <Button
            onClick={() =>
              onFinish({
                to: docAppointment.to,
                from: docAppointment.from,
                doctorId: appointment.doctor._id,
                timeId: appointment.timeSlot._id,
              })
            }
            disabled={appointment.doctor.bookedAppointment.find(
              (ba) => ba.timeSlot === docAppointment._id,
            )}
          >
            {docAppointment.to} - {docAppointment.from}
          </Button>
        ))}
      </div>
    </div>
  );
};

const Appointments = () => {
  const [appointments, setAppointments] = useState([]);
  const [loading, setLoading] = useState(true);
  const { users } = useSelector((state) => state.users);

  useEffect(() => {
    if (users) {
      users.appointments && setAppointments(users.appointments);
      setLoading(false);
    }
  }, [users]);

  return (
    <Layout>
      {loading ? (
        "Loading..."
      ) : appointments.length === 0 ? (
        "No Appointments"
      ) : (
        <React.Fragment>
          {appointments.length === 0 ? (
            "No appointments"
          ) : (
            <div>
              {appointments.map((a, index) => (
                <AppointmentCard key={index} appointment={a} />
              ))}
            </div>
          )}
        </React.Fragment>
      )}
    </Layout>
  );
};

export default Appointments;
