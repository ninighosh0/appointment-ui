import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "antd";
import { hideLoading, showLoading } from "../../redux/alertsSlice";
import { toast } from "react-hot-toast";

const Card = ({ index, a, ap, users, createAppointment = () => {} }) => {
  const [disabled, setDisabled] = useState(
    a.bookedAppointment.find(
      (a) => a.timeSlot.toString() === ap._id.toString(),
    ),
  );
  return (
    <Button
      key={index}
      disabled={disabled}
      onClick={() => {
        !disabled && setDisabled(true);
        createAppointment({
          doctorId: a.userId?._id,
          timeId: ap._id,
          userId: users._id,
        });
      }}
    >
      {ap.to} {ap.from}
    </Button>
  );
};

const BookAppointment = () => {
  const [appointments, setAppointments] = useState([]);
  const { users } = useSelector((state) => state.users);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const getAppointments = async () => {
    try {
      const response = await axios.get("/api/user/get-all-appointments", {
        headers: {
          Authoriation: "Bearer " + localStorage.getItem("token"),
        },
      });
      console.log(response.data.data);
      setLoading(false);
      setAppointments(
        response.data.data.filter((d) => d?.userId?._id !== users._id),
      );
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const createAppointment = async ({
    doctorId = null,
    timeId = null,
    userId = users._id,
  }) => {
    try {
      dispatch(showLoading());
      const response = await axios.post(
        "/api/user/create-appointment",
        { doctorId, timeId, userId },
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      console.log(response.data);
      dispatch(hideLoading());
      toast.success(response.data.message);
      setTimeout(() => window.location.reload(), 1000);
    } catch (error) {
      console.log(error);
      dispatch(hideLoading());
    }
  };

  useEffect(() => {
    if (users) {
      getAppointments();
    }
  }, [users]);

  return (
    <Layout>
      BookAppointment
      <div>
        {loading ? (
          "Loading..."
        ) : appointments.length === 0 ? (
          "No appointments to show!"
        ) : (
          <React.Fragment>
            {appointments.map((a, index) => (
              <div
                key={index}
                style={{
                  border: "1px solid grey",
                  padding: 10,
                  marginBottom: 20,
                  marginTop: 20,
                }}
              >
                Doctor Name: {`${a.userId?.firstName} ${a.userId?.lastName}`}
                <div>
                  Appointments
                  <div>
                    {a.appointment.length === 0
                      ? "No appointment available for this doctor"
                      : a.appointment.map((ap, index) => (
                          <Card
                            key={index}
                            a={a}
                            ap={ap}
                            createAppointment={createAppointment}
                            users={users}
                          />
                        ))}
                  </div>
                </div>
              </div>
            ))}
          </React.Fragment>
        )}
      </div>
    </Layout>
  );
};

export default BookAppointment;
