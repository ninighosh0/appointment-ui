import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "antd";
import { hideLoading, showLoading } from "../../redux/alertsSlice";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

const AppointmentCard = ({ appointment = null }) => {
  const navigate=useNavigate()
 
  return (
    <div
      style={{
        border: "1px solid gray",
        borderRadius: 5,
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        cursor: "pointer",
      }}
    >
      <div style={{ display: "flex", justifyContent: "space-between" }}>
      <div>
       Patient Name:{" "}
       {`${appointment.firstName} ${appointment.lastName}`}
       </div>
      </div>
      <div>
      Doctor Name:{" "}
      {`${appointment.doctorId} ${appointment.doctorId}`}
    </div>
      <div>
        Time Slot: {`${appointment.timeSlot.to} - ${appointment.timeSlot.from}`}
      </div>
      <div>
        <h5 className="mt-3" style={{ fontWeight: "bold" }}>
          View Prescription
        </h5>
        <Button
        onClick={()=>{
          navigate("/edit-prescription")
          localStorage.setItem("prescriptions",JSON.stringify(appointment))
        }
        }
        >
        prescription
        </Button>
      </div>
    </div>
  );
};

const Appointments = () => {
  const [appointments, setAppointments] = useState([]);
  const { users } = useSelector((state) => state.users);
  const [pres,setPres]=useState([]);
  const dispatch = useDispatch();
  const {loading}=useSelector(state=>state)

  const getDatas=async()=>{
    try{
        dispatch(showLoading());
        let dats={};
        if(users && users.role===1)
        {
            dats={
              doctorId:users._id
            }
        }
        const response = await axios.post(
          "/api/user/get-all-prescriptions-user",dats,
          {
            headers: {
              Authoriation: "Bearer " + localStorage.getItem("token"),
            },
          },
        );
      dispatch(hideLoading())
      if(response.data.success)
      {
        setPres(response.data.data)
      }
    }
    catch(err)
    {
      console.log(err)
      dispatch(hideLoading())
      toast.error("could not load data")
    }
  }

  useEffect(() => {
    getDatas()
  },[]);

  console.log(pres,"pres")

  return (
    <Layout>
      {loading ? (
        "Loading..."
      ) : pres.length === 0 ? (
        "No Details"
      ) : (
        <React.Fragment>
          {pres.length === 0 ? (
            "No Details"
          ) : (
            <div>
              {pres.map((a, index) => (
                <AppointmentCard key={index} appointment={a} />
              ))}
            </div>
          )}
        </React.Fragment>
      )}
    </Layout>
  );
};

export default Appointments;
