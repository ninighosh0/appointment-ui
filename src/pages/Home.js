import React, { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/Layout";
import banner from "../assets/banner.png";

const Home = () => {
  const [data, setData] = useState(null);
  const getData = async () => {
    try {
      const response = await axios.post(
        "/api/user/get-user-by-id",
        {},
        {
          headers: {
            Authoriation: "Bearer " + localStorage.getItem("token"),
          },
        },
      );
      console.log(response.data);
      setData(response.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Layout>
      <div style={{ overflow: "hidden" }}>
        <h2>Scheduling Appointment App</h2>
        {data && (
          <div style={{ marginTop: 5 }}>
            <h5>Welcome, {`${data.firstName} ${data.lastName}`}</h5>
            <br />
            <h5>Our Goal</h5>
            <h6>
              Aliquam eros nisl, interdum vitae rhoncus non, aliquam ut quam.
              Nulla efficitur magna sapien. Quisque ut nibh at nunc porta
              mattis. Duis pretium, risus ut pharetra fringilla, nunc mi
              scelerisque erat, eu bibendum nisl nisl non neque. Proin arcu ex,
              viverra vitae scelerisque et, fermentum id urna. Vivamus a orci
              mollis, vehicula ex tempus, gravida nibh. Nulla eros enim,
              facilisis eu sagittis eu, aliquam nec nisi. Etiam nisi tellus,
              laoreet fermentum tempor et, elementum ut erat. Morbi condimentum
              orci sed volutpat varius. Duis placerat elementum dui, eu ultrices
              mauris dignissim quis. Pellentesque rutrum, odio nec sollicitudin
              mattis, nibh massa gravida velit, non imperdiet eros augue eget
              ex. Sed nec dolor nec est malesuada sagittis quis ut ex. Mauris
              ipsum leo, varius at facilisis scelerisque, venenatis non quam.
              Fusce ultrices rhoncus facilisis.
            </h6>
            <div style={{ marginTop: 10 }}>
              <img
                style={{ width: "100%", height: "auto" }}
                src={banner}
                alt="banner"
              />
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default Home;
